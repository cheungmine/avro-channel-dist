*****************************************************
* HOW TO USE avro-channel TO VALIDATE A SIGNUP USER *
*****************************************************
Author: zhangliang

Any questions and proposals are welcome!
You can reached me in the below ways:
    350137278@qq.com
    zhangliang@shenghuo.com

Prerequsities
=============

Linux with java 1.7
-------------------
    Windows not tested


avro-channel-0.0.1-pre-jar-with-dependencies.jar
------------------------------------------------
    If you are using maven, maybe you should install it firstly:

        $ mvn install:install-file \
            -Dfile=/path/to/avro-channel-0.0.1-pre-jar-with-dependencies.jar \
            -DgroupId=avro.channel \
            -DartifactId=avro-channel \
            -Dversion=0.0.1-pre-jar-with-dependencies \
            -Dpackaging=avro-channel-0.0.1-pre-jar-with-dependencies.jar


The Usage for Caller
====================

Start local server for test
---------------------------
  # java -jar /path/to/avro-channel-0.0.1-pre-jar-with-dependencies.jar


A Caller Example
------------------

import avro.channel.user.SignupRequest;
import avro.channel.user.SignupResponse;


void example() {
    // ...

    // Every time the caller MUST create a new SignupRequest as the following line:
    SignupRequest request = new SignupRequest("192.168.122.33", "12343293820281123424424111999");

    // Here we ensure a valid SignupResponse (not Null) can be retrieved in timeout duration
    // The timeout duration and other necessaries are populated in connector-config.xml for client-side(the caller).
    //
    SignupResponse response = SignupRequest.validate(request);

    // We keep the result in SignupResponse returned.
    //
    if (response.getResult() == SignupResponse.ACCEPTED) {
        //TODO: user for signup is accepted by server
    } else if (response.getResult() == SignupResponse.DECLINED) {
        //TODO: user for signup is declined by server
    } else if (response.getResult() == SignupResponse.PENDING) {
        //TODO: server is unavailable
    }

    // ...
}


About Configuration
====================
Default XML config file are provided in jar.

"acceptor-config.xml" is for server (the callee).
"connector-config.xml" is for client (the caller).

acceptor-config.xml is searched by server by sequence:
    1) /etc/avro-channel-config/acceptor-config.xml
    2) /path/to/avro-channel-0.0.1-pre-jar-with-dependencies.jar
    3) /path/to/avro-channel/classes

Client-side config
------------------
    If you want to overide the default config xml file for caller, just
    put a well-formed "connector-config.xml" in folder:
        /etc/avro-channel-config/

Server-side config
------------------
    If you want to overide the default config xml file for caller, just
    put a well-formed "acceptor-config.xml" in folder:
        /etc/avro-channel-config/
